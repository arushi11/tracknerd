import React, { useEffect, useState } from 'react';
import NetInfo from '@react-native-community/netinfo';
import NoInternet from './src/screens/NoInternet';
import { AppNavigator } from './src/navigation/AppNavigator';
import ErrorBoundary from './ErrorBoundary'

const App = () => {
  const [isConnected, setConnection] = useState(true);
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      setConnection(state.isConnected);
    });
    return () => unsubscribe();
  }, []);

  
return (
  <ErrorBoundary>
    {isConnected ? <AppNavigator /> : <NoInternet />}
  </ErrorBoundary>
);
};

export default App;
