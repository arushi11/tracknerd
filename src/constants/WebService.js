export const BASE_URL = 'https://staging-api.tracknerd.io/v1/';
export const BASE_LOGIN_URL = 'auth/login';
export const BASE_VEHICLE_LIST_URL = 'vehicle-groups/vehicles';
