import { DefaultTheme } from 'react-native-paper';

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "#059B9B",
    secondary: '#414757',
    error: '#f13a59',
    grey: '#e2e2e2',
    lightGrey: '#d9dbda',
    darkGrey: '#808080',
  },
};
