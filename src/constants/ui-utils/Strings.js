//Screen names lable
export const LABLE_LOGIN_SCREEN = 'LoginScreen';
export const LABLE_VEHICLE_LIST_SCREEN = 'VehicleScreen';
export const LABLE_MAP_SCREEN = 'MapScreen';

//Error message
export const ERROR_MSG_EMPTY_EMAIL = 'Email Id Required!';
export const ERROR_MSG_INVALID_EMAIL = 'Invalid Email Address!';
export const ERROR_MSG_EMPTY_PASSWORD = 'Password Required!';
export const ERROR_INVALID_PASSWORD =
  'the password is invalid for the given email, or the account corresponding to the email does not have a password set.';
export const MSG_SUCC_LOG = 'You have successfully logged in!';

//Other lable
export const NO_INTERNET = 'No Internet Connection';
export const IS_LOGIN = 'isLogin';
export const BEARER_TOKEN = 'bearerToken';
export const WELCOME_BACK = 'Welcome back';
export const LABLE_LOGIN = 'Login';
export const LABLE_MINI_TRUCK = 'Mini Truck';
export const LABLE_NO_VEHICLE = 'No Vehicle Found';
export const TIME_OUT_1500 = 1500;
export const BACK_PRESS = 'hardwareBackPress';
