import ConstantValue from '../ui-utils/ConstantValue';
import {
  ERROR_MSG_EMPTY_EMAIL,
  ERROR_MSG_EMPTY_PASSWORD,
  ERROR_MSG_INVALID_EMAIL,
} from '../ui-utils/Strings';

export const emailValidator = email => {
  if (!email || email.length <= 0) {
    return ERROR_MSG_EMPTY_EMAIL;
  }
  if (!ConstantValue.regExpValidEmailID.test(email)) {
    return ERROR_MSG_INVALID_EMAIL;
  }

  return '';
};

export const passwordValidator = password => {
  if (!password || password.length <= 0) {
    return ERROR_MSG_EMPTY_PASSWORD;
  }

  return '';
};
