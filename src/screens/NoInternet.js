import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import Background from '../components/Background';
import { NO_INTERNET } from '../constants/ui-utils/Strings';

const NoInternet = () => (
  <Background>
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require('../assets/ic_no_signal.png')}
      />
      <Text style={styles.text}>{NO_INTERNET}</Text>
    </View>
  </Background>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    padding: 30,
    fontSize: 20,
    textAlign: 'center',
  },
  image: {
    width: 120,
    height: 120,
  },
});
export default NoInternet;
