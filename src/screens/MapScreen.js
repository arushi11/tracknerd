import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import {StyleSheet, View, TouchableOpacity, Image,BackHandler} from 'react-native';
import {useRoute} from '@react-navigation/native';
import { BACK_PRESS } from '../constants/ui-utils/Strings';
import { useEffect } from 'react';

const MapScreen = ({navigation}) => {
  let longitude = useRoute().params?.long;
  let latitude = useRoute().params?.lat;

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      BACK_PRESS,
      handleBackButtonClick,
    );

    return () => {
      backHandler.remove();
    };
  }, []);

  const handleBackButtonClick = () => {
    navigation.pop()
    return true;
  };

  return (
    <View style={styles.container}>
      <View style={{zIndex: 10, position: 'absolute', top: -10, left: 5}}>
        <TouchableOpacity
          onPress={() => {
            navigation.pop();
          }}>
          <Image
            style={{width: 30, height: 30, marginTop: 50, marginLeft: 10}}
            source={require('../assets/images/backImg.png')}
          />
        </TouchableOpacity>
      </View>
      <MapView
        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
        style={styles.map}
        region={{
          latitude: latitude,
          longitude: longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}>
        <Marker
          coordinate={{
            latitude: latitude,
            longitude: longitude,
          }}>
          <Image
            source={require('../assets/images/locationImg.png')}
            style={{height: 50, width: 50}}
          />
        </Marker>
      </MapView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default MapScreen;
