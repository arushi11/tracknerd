import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  BackHandler,
  KeyboardAvoidingView,
  Keyboard,
  Image,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';
import SearchBar from '../components/SearchBar';
import RNExitApp from 'react-native-exit-app';
import {
  BACK_PRESS,
  BEARER_TOKEN,
  LABLE_MINI_TRUCK,
  LABLE_NO_VEHICLE,
} from '../constants/ui-utils/Strings';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {getVehicleList} from '../services/ApiService';
import {theme} from '../constants/ui-utils/theme';
import {goToMapScreen} from '../navigation/RootNavigation';
import {ScrollView} from 'react-native-gesture-handler';
import FirebaseConfig from '../firebase/FirebaseConfig';
import {getDatabase, onValue, ref} from 'firebase/database';
import {ActivityIndicator} from 'react-native-paper';

const VehicleListScreen = ({navigation}) => {
  const [searchPhrase, setSearchPhrase] = useState('');
  const [isIndicatorVisible, setIndicatorVisible] = useState(true);
  const [clicked, setClicked] = useState(false);
  const [vehicleData, setVehicleData] = useState([]);
  const [vehicleFilterData, setVehicleFilterData] = useState([]);
  const database = getDatabase(FirebaseConfig);
  useEffect(() => {
    AsyncStorage.getItem(BEARER_TOKEN).then(value => {
      getVehicleListFromAPI(value);
    });

    const backHandler = BackHandler.addEventListener(
      BACK_PRESS,
      handleBackButtonClick,
    );

    return () => {
      backHandler.remove();
    };
  }, []);

  const getVehicleListFromAPI = async _bearerToken => {
    Keyboard.dismiss();
    const tempVehicleList = [];
    const response = await getVehicleList(_bearerToken);
    if (response && response.hasOwnProperty('data')) {
      response.data.map(item => {
        if (
          item &&
          item.hasOwnProperty('vehicles') &&
          item.vehicles.length > 0
        ) {
          item.vehicles.map(list => {
            var updatedList = list;
            updatedList.organisationName = item.name;
            updatedList.vehicleId = list.id;
            updatedList.id = item.id;
            tempVehicleList.push(updatedList);
          });
        }
      });
      setVehicleData(tempVehicleList);
      setVehicleFilterData(tempVehicleList);

    } 
    setIndicatorVisible(false);

  };

  //This function is used for handle back press event
  const handleBackButtonClick = () => {
    RNExitApp.exitApp();
    return true;
  };

  const getVehicleLatLong = vehicleData => {
    console.log(vehicleData);
    const starCountRef = ref(
      database,
      `${vehicleData.vehicleId}-${vehicleData.registrationNumber}/location`,
    );
    onValue(starCountRef, snapshot => {
      const data = snapshot.val();
      if (
        data &&
        data.hasOwnProperty('latitude') &&
        data.hasOwnProperty('longitude')
      ) {
        console.log('Lat', data.latitude);
        goToMapScreen(navigation, {lat: data.latitude, long: data.longitude});
      } else {
        Alert.alert('Vehicle Location cannot be tracked!');
      }
    });
  };

  const searchFilterOnList = value => {
    if (value && value.length > 0) {
      var mainList = [];
      mainList = vehicleData;
      const filterTempList = mainList.filter(item =>
        item.registrationNumber.toLowerCase().includes(value.toLowerCase()),
      );
      setVehicleFilterData(filterTempList);
    } else {
      setVehicleFilterData(vehicleData);
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={'height'}
      style={styles.safeAreaViewContainer}
      keyboardShouldPersistTaps={true}>
      <SafeAreaView style={styles.safeAreaViewContainer}>
        <SearchBar
          searchPhrase={searchPhrase}
          setSearchPhrase={value => {
            setSearchPhrase(value);
            searchFilterOnList(value);
          }}
          clicked={clicked}
          setClicked={setClicked}
        />

        {vehicleFilterData.length === 0 && !isIndicatorVisible ? (
          <Text style={styles.noVehicle}>{LABLE_NO_VEHICLE}</Text>
        ) : (
          <ScrollView>
            <View>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={vehicleFilterData}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => getVehicleLatLong(item)}>
                    <View style={styles.card}>
                      <View style={styles.cardMaincontainer}>
                        <Image
                          style={styles.image}
                          source={
                            item.type === LABLE_MINI_TRUCK
                              ? require('../assets/ic_car.png')
                              : require('../assets/ic_small_car.png')
                          }
                        />
                        <View>
                          <Text style={styles.mainText}>{item.type}</Text>
                          <Text style={styles.orgNameText}>
                            {item.registrationNumber}
                          </Text>
                          <Text style={styles.subTitleText}>
                            {`${item.organisationName} | ${item.id}`}
                          </Text>
                          <Text style={styles.statusText}>{item.status}</Text>
                        </View>
                        <Image
                          style={styles.locationImage}
                          source={require('../assets/images/locationIcon.png')
                          }
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </ScrollView>
        )}
        <ActivityIndicator
          color={theme.colors.primary}
          animating={isIndicatorVisible}
          size={'large'}
          hidesWhenStopped={true}
          style={styles.indicatorStyle}
        />
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  safeAreaViewContainer: {
    flex: 1,
  },
  image: {
    width: 60,
    height: 60,
    marginRight: '10%',
    alignSelf:'center'
  },
  locationImage: {
    width: 40,
    height: 40,
    alignSelf:'center'
  },
  textHeading: {
    fontSize: 18,
    color: theme.colors.primary,
  },
  card: {
    alignSelf: 'center',
    width: '95%',
    backgroundColor: 'white',
    elevation: 5,
    shadowOpacity: 0.25,
    shadowOffset: {height: 2, width: 2},
    padding: 10,
    borderRadius: 5,
    marginVertical: 10,
  },
  cardMaincontainer: {
    flexDirection: 'row',
    justifyContent:'space-evenly'
  },
  mainText: {fontSize: 18, fontWeight: '700'},
  subTitleText: {
    color: theme.colors.darkGrey,
    fontSize: 16,
    fontWeight: '800',
  },
  orgNameText: {
    color: theme.colors.darkGrey,
    fontSize: 16,
    fontWeight: '800',
  },
  statusText: {
    color: 'green',
    fontWeight: '700',
    fontSize: 14,
  },
  noVehicle: {
    fontWeight: '700',
    fontSize: 16,
    alignSelf: 'center',
    marginTop:200
  },
  indicatorStyle: {
    position: 'absolute',
    alignSelf: 'center',
    marginTop: 400,
  },
});
export default VehicleListScreen;
