import React, { memo, useState } from 'react';
import { StyleSheet, View,Image ,KeyboardAvoidingView,ScrollView} from 'react-native';
import Background from '../components/Background';
import Header from '../components/Header';
import Button from '../components/Button';
import CustomTextInput from '../components/CustomTextInput';
import { emailValidator, passwordValidator } from '../constants/utils/utils';
import { theme } from '../constants/ui-utils/theme';
import {
  BEARER_TOKEN,
  ERROR_INVALID_PASSWORD,
  IS_LOGIN,
  LABLE_LOGIN,
  MSG_SUCC_LOG,
  TIME_OUT_1500,
} from '../constants/ui-utils/Strings';
import { ActivityIndicator, Snackbar } from 'react-native-paper';
import { getLoginAuthentication } from '../services/ApiService';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { goToVehicleScreen } from '../navigation/RootNavigation';

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState({
    value: '',
    error: '',
  });
  const [password, setPassword] = useState({
    value: '',
    error: '',
  });
  const [isIndicatorVisible, setIndicatorVisible] = useState(false);
  const [isSnackBarVisible, setSnackBarVisible] = useState(false);
  const [snackBarMsg, setSnackBarMsg] = useState('');

  const onLogin = async () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);

    if (emailError || passwordError) {
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return;
    }
    setIndicatorVisible(true);
    const response = await getLoginAuthentication(email.value, password.value);

    if (response && response.hasOwnProperty('token')) {
      setSnackBarVisible(true);
      setSnackBarMsg(MSG_SUCC_LOG);
      setTimeout(() => {
        setIndicatorVisible(false);
        AsyncStorage.setItem(IS_LOGIN, 'true');
        AsyncStorage.setItem(BEARER_TOKEN, response.token + '').finally(() => {
          goToVehicleScreen(navigation);
        });
      }, TIME_OUT_1500);
    } else {
      setIndicatorVisible(false);
      setSnackBarVisible(true);
      setSnackBarMsg(
        response && response.hasOwnProperty('message')
          ? response.message
          : ERROR_INVALID_PASSWORD,
      );
    }

    setTimeout(() => {
      setSnackBarVisible(false);
      setSnackBarMsg('');
    }, TIME_OUT_1500);
  };

  return (
    <KeyboardAvoidingView style={{flex: 1}} behavior="height">
    <Background>
    <ScrollView keyboardShouldPersistTaps="always">
      <View style={styles.mainContainer}>
        <Header>{LABLE_LOGIN}</Header>
        <View style={{ display: 'flex', justifyContent: 'center',alignItems:"center"}}>
              <Image style={{ width: 100,height:100}} source={require('../assets/images/login.jpeg')} />
            </View>
        <CustomTextInput
          label="Email"
          value={email.value}
          onChangeText={text => setEmail({ value: text, error: '' })}
          error={!!email.error}
          errorText={email.error}
          autoCapitalize="none"
          autoComplete="email"
          textContentType="emailAddress"
          keyboardType="email-address"
          returnKeyType="next"
        />

        <CustomTextInput
          label="Password"
          returnKeyType="done"
          value={password.value}
          onChangeText={text => setPassword({ value: text, error: '' })}
          error={!!password.error}
          errorText={password.error}
          secureTextEntry
        />

        <Button
          mode="contained"
          onPress={onLogin}
          style={styles.buttonContainer}>
          {LABLE_LOGIN}
        </Button>

        <ActivityIndicator
         color={theme.colors.primary}
          animating={isIndicatorVisible}
          size={'large'}
          hidesWhenStopped={true}
          style={styles.indicatorStyle}
        />
        <Snackbar visible={isSnackBarVisible} onDismiss={() => {}}>
          {snackBarMsg}
        </Snackbar>
      </View>
      </ScrollView>
    </Background>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    marginTop: '45%',
    flex: 1,
    width: '100%',
  },
  buttonContainer: {
    marginTop: '10%',
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  label: {
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
  indicatorStyle: {
    position: 'absolute',
    alignSelf: 'center',
    marginTop: 175,
  },
});

export default memo(LoginScreen);