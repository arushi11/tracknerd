import {
  BASE_URL,
  BASE_LOGIN_URL,
  BASE_VEHICLE_LIST_URL,
} from '../constants/WebService';

export const getLoginAuthentication = async (username, password) => {

  const config = {
    method: 'POST',
  };

  return await fetch(
    `${BASE_URL}${BASE_LOGIN_URL}?username=${username}&password=${password}`,
    config,
  )
    .then(response => response.json())
    .then(response => {
      return response;
    })
    .catch(err => {
      return err;
    });
};

export const getVehicleList = async bearer_token => {
  const config = {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + bearer_token,
    },
  };

  return await fetch(`${BASE_URL}${BASE_VEHICLE_LIST_URL}`, config)
    .then(response => response.json())
    .then(response => {
      return response;
    })
    .catch(err => {
      return err;
    });
};
