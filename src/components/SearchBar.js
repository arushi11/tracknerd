import React from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Keyboard,
  Button,
  Image,
  TouchableOpacity,
} from 'react-native';
import {theme} from '../constants/ui-utils/theme';

const SearchBar = ({clicked, searchPhrase, setSearchPhrase, setClicked}) => {
  return (
    <View style={styles.container}>
      <View
        style={clicked ? styles.searchBarclicked : styles.searchBarunclicked}>
        <Image
          style={styles.imageSearch}
          source={require('../assets/images/search.png')}
        />

        <TextInput
          style={styles.input}
          placeholder="Search"
          value={searchPhrase}
          onChangeText={setSearchPhrase}
          onFocus={() => {
            setClicked(true);
          }}
        />

        {clicked && (
          <TouchableOpacity onPress={() => setSearchPhrase('')}>
            <Image
              style={styles.imageClose}
              source={require('../assets/images/close.png')}
            />
          </TouchableOpacity>
        )}
      </View>

      {clicked && (
        <View>
          <Button
            title="Cancel"
            onPress={() => {
              Keyboard.dismiss();
              setClicked(false);
            }}
          />
        </View>
      )}
    </View>
  );
};
export default SearchBar;

// styles
const styles = StyleSheet.create({
  container: {
    margin: 15,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    width: '90%',
  },
  searchBarunclicked: {
    padding: 10,
    flexDirection: 'row',
    width: '100%',
    backgroundColor: theme.colors.lightGrey,
    borderRadius: 15,
    alignItems: 'center',
  },
  searchBarclicked: {
    padding: 10,
    flexDirection: 'row',
    width: '80%',
    backgroundColor: theme.colors.lightGrey,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    paddingHorizontal: 20,
  },
  input: {
    fontSize: 20,
    padding: 5,
    marginLeft: 10,
    width: '90%',
    height: '70%',
  },
  imageSearch: {
    width: 20,
    height: 20,
  },
  imageClose: {
    width: 16,
    height: 16,
  },
});
