import React, { memo } from 'react';
import { StyleSheet, Text } from 'react-native';
import { theme } from '../constants/ui-utils/theme';

const Header = ({ children }) => <Text style={styles.header}>{children}</Text>;

const styles = StyleSheet.create({
  header: {
    fontSize: 26,
    color: theme.colors.primary,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});

export default memo(Header);
