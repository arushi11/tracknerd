import React, {memo} from 'react';
import {StyleSheet, View} from 'react-native';

const Background = ({children}) => (
  <View style={styles.background}>{children}</View>
);

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    height: '100%',
    padding: 20,
  },
});

export default memo(Background);
