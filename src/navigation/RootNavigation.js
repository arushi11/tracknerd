import * as React from 'react';
import { LABLE_MAP_SCREEN, LABLE_VEHICLE_LIST_SCREEN } from '../constants/ui-utils/Strings';
export const navigationRef = React.createRef();


export const goToVehicleScreen = passNavigation => {
  passNavigation.navigate(LABLE_VEHICLE_LIST_SCREEN);
};

export const goToMapScreen = (passNavigation,params) => {
  passNavigation.navigate(LABLE_MAP_SCREEN,params);
};
