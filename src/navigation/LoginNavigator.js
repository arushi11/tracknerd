import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screens/LoginScreen';
import VehicleListScreen from '../screens/VehicleList';
import {
  LABLE_LOGIN_SCREEN,
  LABLE_VEHICLE_LIST_SCREEN,LABLE_MAP_SCREEN
} from '../constants/ui-utils/Strings';
import MapScreen from '../screens/MapScreen';

const LoginStack = createStackNavigator();
export const LoginStackScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen
      name={LABLE_LOGIN_SCREEN}
      component={LoginScreen}
      options={{ headerShown: false }}
    />
    <LoginStack.Screen
      name={LABLE_VEHICLE_LIST_SCREEN}
      component={VehicleListScreen}
      options={{ headerShown: false }}
    />
      <LoginStack.Screen
      name={LABLE_MAP_SCREEN}
      component={MapScreen}
      options={{ headerShown: false }}
    />
  </LoginStack.Navigator>
);
