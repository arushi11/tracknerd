import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from './RootNavigation';
import { LoginStackScreen } from './LoginNavigator';


export const AppNavigator = () => {

  return (
    <NavigationContainer ref={navigationRef}>
      <LoginStackScreen />
    </NavigationContainer>
  );
};
